# colormap.pf
# aym3ric-at-goto10-dot-org
# a linear colormap generator that can be also used to 
# produce any sort of linear matrix based LUTs
#
# colormap takes a list of ranges as input
# a range is defined as a key couple and number of step to
# interpolate from the 1st to the 2nd
# range = ((start value) number of steps ))
#
# example:  
# to generate a 3 keys 8bits colormap
#
# ( ((1 0 0) (0 1 0) 127)
#   ((0 1 0) (0 0 1) 128) ) colormap
# 
# note: 1st key is counted out of the step total so to obtain
#       256 values you need to have a total of 255 steps in 
#	the input list.


load-matrix

0 variable! diff
0 variable! totalstep	

: key>mtx	1 pack >matrix ;
: mtx>key	>list unpack ;
: ABdiff	key>mtx >r key>mtx r> - ;
: rangedup 	3 pack dup unpack ; 

: mtx-step			# compute diff between 2 keypoints
	>r
	ABdiff
	r> >float 1. swap / *
	diff !
	;

: initial-key
	split nip		# extract 1st range
	split nip		# extract A key
	;

: mtx-interpolate			
	reverse unpack			# reformat data
	rangedup			# dup the original range
	mtx-step
	unpack				# unpack the dup'ed range
	nip				# drop the B key
	swap key>mtx swap		# convert A key into matrix
	dup totalstep @ + totalstep !	# update total stepx 
	for 
		diff @ +		# generate interpolation 
		dup mtx>key swap	# convert the key into list
	next			 
	drop				# drop the leftover
	;

: colormap 
	dup initial-key swap		
	0 totalstep !
	' mtx-interpolate for-each 
	1 totalstep @ + pack reverse
	>matrix
	;
