#!/bin/bash

function mainmenu {
cp screenshot-original.pf screenshot-real.pf
cp temp-store-original.pf temp-store-real.pf
cp temp-store-original.pf temp-store.pf
cp screenshot-original.pf screenshot.pf

echo "metabiosis"
	dialog --title "herbarium START " --ok-label "  START  " --msgbox "\n\n
          --------------------------------------------\n
             M   E   T   A   B   I   O   S   I   S      \n                               
          --------------------------------------------\n
           db   db d8888b. d8888b. d8888b. .88b  d88.\n
           88   88 88  \`8D 88  \`8D 88  \`8D 88'YbdP\`88\n
           88ooo88 88oobY' 88oooY' 88oobY' 88  88  88\n
           88~~~88 88\`8b   88~~~b. 88\`8b   88  88  88\n
           88   88 88 \`88. 88   8D 88 \`88. 88  88  88\n
           YP   YP 88   YD Y8888P' 88   YD YP  YP  YP\n
          --------------------------------------------\n
                P  R  E  S  S   .   S  T  A  R  T     \n
          --------------------------------------------\n    
" 20 70 
sel=$?
 case $sel in
    0) seed ;;
 esac
}											
# ZAADJE
function seed {
	dialog --cancel-label "terug naar START" --inputbox "\
 db\n 
o88 \n
 88 \n
 88 \n
 88 \n
 VP \n\nKunstmatige planten groeien, net als echte planten, uit 1 zaadje. Het zaadje is een nummer tussen de 0 en 7.\n\n\n\nGeef jou plant hier een zaadje waaruit het kan groeien:\n" 20 70  2> .temp
	 sel=$?
	 sed -i "s/[ \t]*$//" .temp
	 zaadje=`cat .temp`
	 rm -f .temp
	 if test $sel -eq 1 ; then
	 	mainmenu
	elif test $sel -eq 0 ; then
		if [ -n "$zaadje" ] ; then
			if [[ $zaadje = [0-7] ]]; then
			dna-plant
			else dialog --infobox "vul een nummer in tussen 0 en 7! Jij vulde $zaadje in" 20 70
			sleep 2
			seed
			fi
		else dialog --infobox "je hebt niks ingevuld, probeer het nog een keer" 20 70
		sleep 2
		seed
		fi	
	 fi
	 }
# DNA VAN PLANT
function dna-plant {
	dialog --cancel-label "terug naar START" --inputbox "\
.d888b. \n
VP  \`8D \n
   odD' \n
  .88'   \n
j88.    \n
888888D \n\nNu is het tijd om het "DNA" van je plant te ontwerpen.\nEr zijn 5 regels: maximaal 15 cijfers, alleen cijfers tussen 0 en 7, laat een spatie tussen alle cijfers, en na elke 4, moet je later in de lijst ook een 5 gebruiken\n\nEen voorbeeld: 1 4 3 1 1 5 1 4 0 1 1 5 4 1 5" 20 70  2> .temp
	 sel=$?
	 dna=`cat .temp`
	 size=`sed "s/\ //g" .temp`
	 amount=`expr length $size` 
	 rm -f .temp
	 if test $sel -eq 1 ; then
	 	mainmenu
	elif test $sel -eq 0 ; then
		if [ -n "$dna" ] ; then
			if test $amount -gt 15 ; then
			dialog --infobox "het maximum aantal nummers is 15! Jij vulde $amount in. Probeer het nog eens" 20 70 
			sleep 2
			dna-plant
			else
			for i in $dna ; do
				if test $i -gt 7 ; then
				dialog --infobox "kies alleen nummers tussen 0 en 7! probeer het nog een keer" 20 70
				sleep 2
				dna-plant
				fi
				if [[ $i = [0-7] ]]; then
				echo ""
				else
				dialog --infobox "kies alleen nummers tussen 0 en 7! probeer het nog een keer" 20 70
				sleep 2
				dna-plant
				fi
			done
			fi
		else dialog --infobox "je hebt niks ingevuld, probeer het nog een keer" 20 70
		sleep 2
		dna-plant
		fi	
	 fi
	 iteration
	 }

# DNA : AANTAL HERHALINGEN
function iteration {
	dialog --cancel-label "terug naar START" --inputbox "\
d8888b.\n 
VP  \`8D \n
  oooY' \n
  ~~~b. \n
db   8D \n
Y8888P'\n\nMet het 'DNA' en het zaadje kan je plant beginnen te groeien. De plant kan klein blijven of heel groot worden, dit ligt aan hoevaak de plant zijn vorm herhaalt. Hoevaak moet jou plant zijn basisvorm herhalen? \n\nVul een nummer tussen de 1 en 6 in" 20 70  2> .temp
	 sel=$?
	 sed -i "s/[ \t]*$//" .temp
	 herhaling=`cat .temp`
	 rm -f .temp
	 if test $sel -eq 1 ; then
	 	mainmenu
	elif test $sel -eq 0 ; then
		if [ -n "$herhaling" ] ; then
			if [[ $herhaling = [1-6] ]]; then
			pencil-size
			else dialog --infobox "vul een nummer in tussen 1 en 6! Jij vulde $herhaling in" 20 70
			sleep 2
			iteration
			fi
		else dialog --infobox "je hebt niks ingevuld, probeer het nog een keer" 20 70
		sleep 2
		iteration
		fi	
	 fi
	 }
# DNA : DIKTE VAN DE STENGEL
function pencil-size {
	dialog --cancel-label "terug naar START" --inputbox "\
  j88D  \n
 j8~88  \n
j8' 88  \n
V88888D \n
    88  \n
    VP  \n\nKies de dikte van de stengel van de plant. Vul een nummer in tussen 1 en 9. \n\n\n\n 9 is heel erg dik en 1 is heel erg dun" 20 70  2> .temp
	 sel=$?
	 pencil_size=`cat .temp`
	 rm -f .temp
	 if test $sel -eq 1 ; then
	 	mainmenu
	elif test $sel -eq 0 ; then
		if [ -n "$pencil_size" ] ; then
			if [[ $pencil_size = [0-9] ]]; then
				echo ""
				else
				dialog --infobox "kies alleen nummers tussen 1 en 9! probeer het nog een keer" 20 70
				sleep 2
				pencil-size
				fi
			if test $pencil_size -eq 0 ; then
			dialog --infobox "vul een nummer in tussen 1 en 9! Jij vulde $pencil_size in" 20 70
			sleep 2
			pencil-size						 
			elif test $pencil_size -gt 9 ; then
			dialog --infobox "vul een nummer in tussen 1 en 9! Jij vulde $pencil_size in" 20 70
			sleep 2
			pencil-size
			else 
			color-red
			fi
		else dialog --infobox "je hebt niks ingevuld, probeer het nog een keer" 20 70
		sleep 2
		pencil-size
		fi	
	 fi
	 }
# DNA : KLEUR VAN DE PLANT
function color-red {
	dialog --cancel-label "terug naar START" --inputbox "\
  ooooo\n 
 8P~~~~ \n
dP      \n
V8888b. \n
   \`8D \n
88oobY'\n\nHier kun je de kleur van je plant bepalen. Kies een getal tussen de 0 en 9 voor rood, groen en blauw. 0 betekend niks en 10 heel veel van een kleur. De computer mengt ze door elkaar.\n\n\nVul hier een getal in voor ROOD:" 20 70  2> .temp
	 sel=$?
	 red=`cat .temp`
	 rm -f .temp
	 if test $sel -eq 1 ; then
	 	mainmenu
	elif test $sel -eq 0 ; then
		if [ -n "$red" ] ; then
			if [[ $red = [0-9] ]]; then
			color-green
			else
			dialog --infobox "kies alleen nummers tussen 0 en 9! Jij vulde $red in. Probeer het nog een keer" 20 70
			sleep 2
			color-red
			fi
		else dialog --infobox "je hebt niks ingevuld, probeer het nog een keer" 20 70
		sleep 2
		color-red
		fi
	 fi
	 }
function color-green {
	dialog --cancel-label "terug naar START" --inputbox "\
   dD  \n 
  d8'   \n
 d8'    \n
d8888b. \n
88' \`8D \n
\`8888P  \n\nHier kun je de kleur van je plant bepalen. Kies een getal tussen de 0 en 9 voor rood, groen en blauw.\n\n\n\nVul hier een getal in voor GROEN:" 20 70  2> .temp
	 sel=$?
	 green=`cat .temp`
	 rm -f .temp
	 if test $sel -eq 1 ; then
	 	mainmenu
	elif test $sel -eq 0 ; then
		if [ -n "$green" ] ; then
			if [[ $green = [0-9] ]]; then
			color-blue
			else
			dialog --infobox "kies alleen nummers tussen 0 en 9! Jij vulde $green in. Probeer het nog een keer" 20 70
			sleep 2
			color-green
			fi
		else dialog --infobox "je hebt niks ingevuld, probeer het nog een keer" 20 70
		sleep 2
		color-green
		fi
	 fi
	 }
function color-blue {
	dialog --cancel-label "terug naar START" --inputbox "\
d88888D \n
VP  d8' \n
   d8'  \n
  d8'   \n
 d8'    \n
d8'     \n\nHier kun je de kleur van je plant bepalen. Kies een getal tussen de 0 en 10 voor rood, groen en blauw.\n\n\n\nVul hier een getal in voor BLAUW:" 20 70  2> .temp
	 sel=$?
	 blue=`cat .temp`
	 rm -f .temp
	 if test $sel -eq 1 ; then
	 	mainmenu
	elif test $sel -eq 0 ; then
		if [ -n "$blue" ] ; then
			if [[ $blue = [0-9] ]]; then
			branch-angle
			else
			dialog --infobox "kies alleen nummers tussen 0 en 9! Jij vulde $blue in. Probeer het nog een keer" 20 70
			sleep 2
			color-blue
			fi
		else dialog --infobox "je hebt niks ingevuld, probeer het nog een keer" 20 70
		sleep 2
		color-blue
		fi
	 fi
	 }

# DNA : HOEK VAN DE TAKKEN
function branch-angle {
	dialog --cancel-label "terug naar START" --inputbox "\
.d888b. \n
88   8D \n
\`VoooY' \n
.d~~~b. \n
88   8D \n
\`Y888P' \n\nDe laatste keuze voordat je je plant kunt zien: geef hier aan hoe groot de hoek tussen de takken moet zijn.\n\n\n\nVul een getal in tussen de 0 en 360 :" 20 70  2> .temp
	 sel=$?
	 angle=`cat .temp`
	 rm -f .temp
	 if test $sel -eq 1 ; then
	 	mainmenu
	elif test $sel -eq 0 ; then
		if [ -n "$angle" ] ; then
#			for i in $angle ; do
#				if [[ $i = [0-9] ]]; then
#				echo ""
#				else
#				dialog --infobox "kies alleen nummers tussen 0 en 360! Jij vulde $angle in. Probeer het nog een keer" 20 70
#				sleep 2
#				branch-angle
#				fi
#			done
			if test $angle -gt 360 ; then
			dialog --infobox "vul een nummer in tussen 0 en 360! Jij vulde $angle in" 20 70
			sleep 2
			branch-angle
			else draw-it
			fi
		else dialog --infobox "je hebt niks ingevuld, probeer het nog een keer" 20 70
		sleep 2
		branch-angle
		fi
	 fi
	 }
# TEKEN DE PLANT IN PF 
function draw-it {
		sed -i '2d' temp-store.pf
		sed -i "/~ZAADJE/a\ \($zaadje\) zaadje !" temp-store.pf
		sed -i '4d' temp-store.pf
		sed -i "/~DNA/a\ \($dna\) axiom !" temp-store.pf
		sed -i '6d' temp-store.pf
		sed -i "/~HERHALING/a\ $herhaling T !" temp-store.pf
		sed -i '8d' temp-store.pf
		sed -i "/~PENCIL_SIZE/a\ $pencil_size. PENCIL_SIZE !" temp-store.pf
		sed -i '10d' temp-store.pf
		sed -i "/~RGBA/a\ \( $red. $green. $blue. 4. \) RGBA !" temp-store.pf
		sed -i '12d' temp-store.pf
		sed -i "/~ANGLE/a\ $angle. new_ANGLE !" temp-store.pf
		sed -i '16d' temp-store.pf
		sed -i "/~GATE/a\ 1 GATE !" temp-store.pf
		sed -i '18d' temp-store.pf
		sed -i "/~RESET/a\ 1 RESET !" temp-store.pf

		# copy the file over in 1 time to avoid conflicts
		cp temp-store.pf temp-store-real.pf
	  		 
	dialog --no-label "terug naar START" --yes-label "SAVE" --yesno "Je plant wordt nu getekend...\n\n\nWACHT TOT HIJ KLAAR IS MET GROEIEN!\n\n\nAls je tevreden bent met je plant, en je wilt hem in herbarium opslaan, klik dan op 'SAVE'.\n\nAls je niet tevreden bent, en je wilt een nieuwe plant ontwerpen, klik dan op 'terug naar START'" 20 70 
	 sel=$?
		if test $sel -eq 1 ; then
	 	mainmenu
	elif test $sel -eq 0 ; then
		sleep 1
		name
	fi
	}

# SAVING THE NAME OF THE AUTHOR
function name {
	dialog --no-cancel --inputbox "Om je plant te archiveren, hebben we 3 dingen nodig:\n\n\n
	1. je naam\n
	2. de naam van je plant\n
	3. een korte omschrijving van je plant\n\n\n\n\n\n\nVul hier je naam in:" 20 70  2> .temp
	 sel=$?
	 maxlen="17"
	 theword=`cat .temp`
	 testofset="0"
	 if [ ${#theword} -gt $maxlen ]
	 	then
			name="${theword:$testofset:$maxlen}${truncsymbol}"
		else
			name=$theword
	fi
		
	 rm -f .temp
	 echo $name > .temp
	 sed -i 's/ /_/g' .temp 
	 name2=`cat .temp`
	 rm -f .temp
	 if test $sel -eq 0 ; then
		if [ -n "$name" ] ; then
			plantname
		else dialog --infobox "je hebt niks ingevuld, probeer het nog een keer" 20 70
		sleep 2
		name
		fi
	 fi
	 }
# SAVING THE NAME OF THE PLANT
function plantname {
	dialog --no-cancel --inputbox "Bedenk een naam voor je plant. Jou plant wordt onder die naam gearchiveerd in het herbarium.\n\n\n\n\n\n\n\n\n\n\nVul hier de naam van je plant in:" 20 70  2> .temp
	 sel=$?
	 maxlen="17"
	 theword=`cat .temp`
	 testofset="0"
	 if [ ${#theword} -gt $maxlen ]
	 	 then
		 	plantname="${theword:$testofset:$maxlen}${truncsymbol}"
		else
			plantname=$theword
	fi
	rm -f .temp
	echo $plantname > .temp
	 sed -i 's/ /_/g' .temp 
	 plantname2=`cat .temp`
	 rm -f .temp
	 if test $sel -eq 0 ; then
		if [ -n "$plantname" ] ; then
			screenshot
		else dialog --infobox "je hebt niks ingevuld, probeer het nog een keer" 20 70
		sleep 2
		plantname
		fi
	 fi
	 }
# TAKING A SCREENSHOT
function screenshot {
	dialog --no-cancel --inputbox "Geef een korte beschrijving van je plant (maximaal 2 zinnen).\n\n\n\n\n\n\n\n\n\n\nJe kunt bijvoorbeeld iets vertellen over waar hij groeit, in wat voor computer, of hij bloeit en hoe, etc." 20 70  2> .temp
	 sel=$?
	 description=`cat .temp`
	 rm -f .temp
	 if test $sel -eq 0 ; then
		if [ -n "$description" ] ; then
			# store name and plantname in the temp-store file
			sed -i '2d' screenshot.pf
			sed -i "/~NAME/a\ \"$name2\" NAME !" screenshot.pf
			sed -i '4d' screenshot.pf
			sed -i "/~PLANTNAME/a\ \"$plantname2\" PLANTNAME !" screenshot.pf
			# tell pf to make a screenshot 
			sed -i '6d' screenshot.pf
			sed -i "/~SCREENSHOT/a\ 1 SCREENSHOT !" screenshot.pf
			
			# to avoid conflicts, copy over the file in one time
			cp screenshot.pf screenshot-real.pf
			 
			# write description, plantname and name of author to a file
			echo "$name" >> $name2$plantname2.txt
			echo "$plantname" >> $name2$plantname2.txt
			echo "$description" >> $name2$plantname2.txt
			echo "$zaadje" "-&gt" "$dna" >> $name2$plantname2.txt
			goodbye
		else dialog --infobox "je hebt niks ingevuld, probeer het nog een keer" 20 70
			sleep 2
		screenshot
		fi
	 fi
		}

# THANK YOU AND GOODBYE SCREEN
function goodbye
	{
	dialog --infobox "De plant $plantname wordt bewaard in het herbarium.\n\nBedankt voor je bijdrage $name!"\
	 20 70
	sleep 4
	mainmenu
	 }

# (xterm -e pf MAIN.pf)&
mainmenu
