#!/bin/sh 
# OKI 320 ML tests
# this test is to generate random 8 pin mode patterns

PRINT="lp -o raw"
INIT="@59"
#echo -e "${INIT}" | $PRINT

rm  /tmp/oki-job-graph
touch /tmp/oki-job-graph

#PRNG
function rand {
    echo "obase=8;`expr $RANDOM % 256`" | bc
} 

function randpat {
    n=1
    while [ $n -le 256 ]; do
	RND=`rand`
	echo -ne '\'$RND
	n=$((n+1))
    done
}

#echo -e `randpat`

# This is for quadruple density
# ESC Z 96 0
PATTERN=`randpat`
PATTERN2=`head /dev/urandom -c 1024`
#
# echo -e 'Z\140\0'$PATTERN > /tmp/oki-job-graph
echo -e '@59*1\0\4'$PATTERN2'@' > /tmp/oki-job-graph
lp -o raw /tmp/oki-job-graph
