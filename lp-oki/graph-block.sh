#!/bin/sh 
# OKI 320 ML tests
# this test is to generate random 8 pin mode patterns

PRINT="lp -o raw"
INIT="@59"
#echo -e "${INIT}" | $PRINT

rm  /tmp/oki-job-block
touch /tmp/oki-job-block

#PRNG
function rand {
    echo "obase=8;`expr $RANDOM % 256`" | bc
} 

function randpat {
    n=1
    while [ $n -le 256 ]; do
	RND=`rand`
	echo -ne '\'$RND
	n=$((n+1))
    done
}

#echo -e `randpat`

# This is for quadruple density
# ESC Z 96 0
PATTERN=``
PATTERN2=`tail ~/myblock-full.fb -c 1024 | sed 's/\x00/ /g' | sed 's/ /\x02/g'`
#
# echo -e 'Z\140\0'$PATTERN > /tmp/oki-job-graph
echo -e '@59*3\0\4'"$PATTERN2"'@' > /tmp/oki-job-graph
lp -o raw /tmp/oki-job-graph
