#!/bin/sh 
# OKI 320 ML tests
# this test prints the 256 "8 pin mode" characters

PRINT="lp -o raw"
INIT="@59"
#echo -e "${INIT}" | $PRINT

rm  /tmp/oki-job-graph-all
touch /tmp/oki-job-graph-all 

function counting {
    n=1
    while [ $n -le 256 ]; do
	COUNTER=`echo "obase=8;$n" | bc`
	echo -ne '\'$COUNTER
	#echo -ne '\'6
	n=$((n+1))
    done
}

PATTERN=`counting`
echo -e '@59*1\0\2'${PATTERN}${PATTERN}'@' > /tmp/oki-job-graph-all
lp -o raw /tmp/oki-job-graph-all