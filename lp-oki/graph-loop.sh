#!/bin/sh 
# OKI 320 ML tests
# this test prints loops of 2 characters

PRINT="lp -o raw"
INIT="@59"
#echo -e "${INIT}" | $PRINT

rm  /tmp/oki-job-graph-loop
touch /tmp/oki-job-graph-loop 

function randbytes {
    echo "obase=8;`expr $RANDOM % 7 \* 13`" | bc # BASH method
    
    #head /dev/urandom -c 2 | sed 's/\x00/ /g' # URANDOM method
}

function bytesloop {
    
    # BYTESSEQ='\'`randbytes`'\'`randbytes`
    
    BYTESSEQ='\'`randbytes`
    BYTESSEQ="${BYTESSEQ}""${BYTESSEQ}"

    #BYTESSEQ="`randbytes`" # URANDOM method

    n=1
    while [ $n -le 2 ]; do
	BYTESSEQ="${BYTESSEQ}""${BYTESSEQ}"
	n=$((n+1))
    done
    # echo -ne "${BYTESSEQ}" | hexdump -C
}

function bytesline {
    PATTERN=""
    i=1
    while [ $i -le 128 ]; do
	bytesloop
	PATTERN="${PATTERN}""${BYTESSEQ}"
	i=$((i+1))
    done
    echo -ne "${PATTERN}" | hexdump -C
}

bytesline
echo "${PATTERN}" > pat


echo -e '@59*1\0\4'"${PATTERN}"'@' > /tmp/oki-job-graph-loop
lp -o raw /tmp/oki-job-graph-loop