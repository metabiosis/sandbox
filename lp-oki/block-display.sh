#!/bin/sh 
# display the block image population
# 

PRINTER=oki
BLOCKFB=/pool/hello-process.fb
BLOCKCP=/pool/hello-process-copy.fb
EMPTYCHAR="XXXXXX"

# safety first
function prepare {
    cp ${BLOCKFB} ${BLOCKCP}
}

# byte extraction
function extchar {
    head -c $1 ${BLOCKCP} | tail -c 1
}

# byte filtering
function printchar {
    if [ "`extchar $1`" > 0 ]
    then
        if [ "`extchar $1`" != " " ]
	then
	    BYTECHAR=`extchar $1`
	    echo -ne "${BYTECHAR}${BYTECHAR}${BYTECHAR}${BYTECHAR}${BYTECHAR}${BYTECHAR}"
	else
	    echo -ne ${EMPTYCHAR}
	fi
    else
	echo -ne ${EMPTYCHAR}
    fi
}

# block parsing
function dumpblock {
    for i in `seq 1 128`
    do
	#echo -n "$i: " # DEBUG
	printchar `expr $i \* 1024 + 3`
    done
}

prepare

PATTERN=`dumpblock`

# block printing
echo -e '@59'"\t"'*1\0\3'"${PATTERN}"'@' | lp -o raw