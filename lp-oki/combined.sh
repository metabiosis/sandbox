#!/bin/sh 
# OKI 320 ML tests
# this test prints all the available combined mode

PRINT="lp -o raw"
INIT="@59"
echo "${INIT}Combined Modes Listing"  | $PRINT

# escape and run to the hills
# '^[@^[5^[!\64hello'

# multi jobs version
#X=0
#while [ $X -le 255 ]; do
#    octalX=`echo "obase=8;$X" | bc`
#    echo -e '@59'"mode $X: "'!\'$octalX'hello process' | $PRINT
#    X=$((X+1))
#done

# single job version
rm  /tmp/oki-job-combined
touch /tmp/oki-job-combined
X=0
while [ $X -le 255 ]; do
    octalX=`echo "obase=8;$X" | bc`
    echo -e '@59'"mode $X: "'!\'$octalX'hello process' >> /tmp/oki-job-combined
    X=$((X+1))
done

lp -o raw /tmp/oki-job-combined