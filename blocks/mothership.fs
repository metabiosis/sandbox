\ This is the mothership, it controls the execution of the blocks
\ load me using require mothership.fs at the interactive prompt
\ or start gforth with me

\ The mothership loads all needed files and starts the execution of 
\ block 1, after it is executed, it loads block 2, then block 3, etc.
\ The initialisation of the blocks will be a random spreading of x 
\ basic programs over the total n blocks.

\ Init random number generator
require random.fs
here seed !

\ Load block editor and blockfile used to store the programs
use blocked.fb
1 load 2 load 3 load
use hello-process.fb

\ Global variables
variable current-b
variable target-b
variable count
variable size
variable speed
variable time

\ Set variables
1 count !
1000 speed !
15 size !
0 time !

\ Load initialisation files
include dictionary.fs
include initialize-blocks.fs

\ Main loop
: loopy
    for
    count @ size @ 1 + mod dup .
    current-b !  
    empty? 
    counter 
    next 
;

: main
    begin
	size @ loopy
	speed @ ms
	time @ 1 + time !
	." -------------------" time @ . cr
    again
;

\ START THE ENGINES!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
update flush
size @ 1 - loopy
\ main
