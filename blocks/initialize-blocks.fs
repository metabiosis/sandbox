\ This file initialises the process by randomly seeding x programs into x blocks

\ INDEX BLOCK used to identify blocks. variable 'target' is set to 0 for empty,
\ 1 for copycat, 2 for eraser head, etc. 
size @ 1 + l
0 t       ( 0: empty)
1 t ( c)  ( 1: copy cat)
2 t ( z)  ( 2: eraser head)
3 t ( !)  ( 3: warlord)
4 t ( r)  ( 4: anti-eraser)

\ "copy-cat"
b-empty?
current-b @ l
0 t ( c)
1 t  ." copy-cat "  
2 t  neighbour-check-new
3 t  copy-ca

\ "eraser head"
b-empty?
current-b @ l
0 t ( z)
1 t  ." eraser "
2 t  neighbour-check-new 
3 t  eraser-ca

\ "anti-emptiness-warlord" (0 is empty, 1 is full)
b-empty?
current-b @ l
0 t ( !) 
1 t  ." warlord " 
2 t  neighbour-check-new 
3 t  warlord-ca

\ "anti-eraser"
b-empty?
current-b @ l
0 t ( r)
1 t  ." anti-eraser "
2 t  neighbour-check-new
3 t  anti-eraser-ca

update flush
